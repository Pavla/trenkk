myapp.directive("fileread", [function () {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                        scope.fileread = loadEvent.target.result;
                    });
                }
                reader.readAsDataURL(changeEvent.target.files[0]);
            });
        }
    }
}]);

myapp.directive('datetimez', function() {
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.datetimepicker({
           format: "dd/MM/yyyy",
           viewMode: "years", 
           pickTime: false,
           autoclose: true
          }).on('changeDate', function(e) {
              
           // FORMATTING  Wed Jul 17 2013 00:00:00 GMT+0000 TO dd/MM/yyyy
           // date = this.getDate()            // UTC time 
           // localDate = this.getLocalDate()  // user local time
           var outputDate = new Date(e.localDate);
           
           var d = ("0" + outputDate.getDate()).slice(-2);  //-2 for 01 date, and 023 gives 23
           var m = ("0" + (outputDate.getMonth()+1)).slice(-2); //+1 cause months starts with 0
           var y = outputDate.getFullYear(); 

           var n = d + "/" + m + "/" + y;
              
            //ngModelCtrl.$setViewValue(e.date);
            ngModelCtrl.$setViewValue(n);
            scope.$apply();
          });
        }
    };

});

myapp.directive('datetimez2', function() {
    
    return {
        restrict: 'A',
        require : 'ngModel',
        link: function(scope, element, attrs, ngModelCtrl) {
          element.datetimepicker({
           format: "dd/MM/yyyy",
           viewMode: "months", 
           pickTime: false,
           endDate: new Date(), // date restriction by today's date
           autoclose: true
            }).on('changeDate', function(e) {

                    // FORMATTING  Wed Jul 17 2013 00:00:00 GMT+0000 TO dd/MM/yyyy
                    // localDate = this.getLocalDate()  // UTC time
                    // date = this.getDate()            // user local time
                    var outputDate = new Date(e.localDate); 

                    var d = ("0" + (outputDate.getDate())).slice(-2);  //-2 for 01 date, and 023 gives 23
                    var m = ("0" + (outputDate.getMonth() + 1)).slice(-2); //+1 cause months starts with 0
                    var y = outputDate.getFullYear();

                    var n = d + "/" + m + "/" + y;

                    //ngModelCtrl.$setViewValue(e.date);
                    ngModelCtrl.$setViewValue(n);
                    scope.$apply();
                //}
          });
        }
    };
});

myapp.directive('input', function() {
    return {
        restrict: 'E',
        require: '?ngModel',
        link: function(scope, elm, attr, ctrl) {
            if (!ctrl) {
                return;
            }
            
            elm.bind('focus', function () {
                elm.addClass('has-focus');

                scope.$apply(function () {
                    ctrl.hasFocus = true;
                });
            });

            elm.bind('blur', function () {
                elm.removeClass('has-focus');
                elm.addClass('has-visited');

                scope.$apply(function () {
                    ctrl.hasFocus = false;
                    ctrl.hasVisited = true;
                });
            });
           
            if (attr.type === 'text' && attr.ngPattern === '/[0-9]/') {
                elm.bind('keyup',function (){
                    var text = this.value;
                    console.log(text);
                    this.value = text.replace(/[a-zA-Z]/g,'');
                });
            }  
        }
    };
});