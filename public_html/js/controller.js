//GLOBAR VAR
var imagePath = "http://localhost/app/public_html/backend/testimages/";
var backendBaseUrl = "http://localhost/app/public_html/backend/rest.php/";

myapp.controller('membersController', ['$scope', '$http', function($scope, $http, $stateParams) {

        //perfect-scrollbar
        $scope.$on('$viewContentLoaded', function(event) {
            $('.SuppressScrollX').perfectScrollbar({suppressScrollX: true});
        });

        //$scope.getUsers = function() {

        $http({
            method: 'GET',
            url: backendBaseUrl + 'users'
        })
                .success(function(data, status, headers, config) {	    
                    $scope.users = data;
		    $scope.imageurl = imagePath;
                })
                .error(function(data, status, headers, config) {
                    alert("Greska pri dohvacanju liste korinika");
                })
                ;
        //};

}]);

myapp.controller('membersNewController', ['$scope', '$http', function($scope, $http) {

    //perfect-scrollbar
    $scope.$on('$viewContentLoaded', function(event) {
        $('.SuppressScrollX').perfectScrollbar({suppressScrollX: true});
    });

    $scope.addUser = function() {

        var postData = new Object();
        postData.name = $scope.userNameInput;
        postData.surname = $scope.userSurnameInput;
        postData.gender = $scope.userGenderInput;
        postData.birth = $scope.userBirthInput;
        postData.email = $scope.userEmailInput;
        postData.phone = $scope.userPhoneInput;
        postData.city = $scope.userCityInput;
        postData.state = $scope.userStateInput;
        postData.profession = $scope.userProfessionInput;
        postData.relationship = $scope.userRelationshipInput;
        postData.weight = $scope.userWeightInput;
        postData.history = $scope.userHistoryInput;
        postData.remarks = $scope.userRemarksInput;
        postData.notes = $scope.userNotesInput;
        
        //alert(postData.birth);
        //alert(postData.name);
	//alert($scope.uploadme);
	//if($scope.uploadme != null)
	postData.base64image = $scope.uploadme;

        $http({method: 'POST',
            url: backendBaseUrl + 'users',
            data: postData})
                .success(function(data, status, headers, config) {
                    //postData.id = data; // surplus because push(data[0])
                    alert("Dodan je novi korisnik");
                    $scope.users.push(data[0]);

                })

                .error(function(data, status, headers, config) {
                    console.log("Niste unijeli sve podatke/greska pri dodavanju korisnika");
                });
    };

}]);


myapp.controller('membersDetailsController', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {
    
    //perfect-scrollbar
    $scope.$on('$viewContentLoaded', function(event) {
        $('.SuppressScrollX').perfectScrollbar({suppressScrollX: true});
    });

    $scope.user = null;
    //alert("Korisnik je dodan");

    $http({
        method: 'GET',
        url: backendBaseUrl + 'users/' + $stateParams.id})

            .success(function(data, status, headers, config) {
                $scope.user = data[0];
		$scope.imageurl= imagePath;
            })
            .error(function(data, status, headers, config) {
                alert("Dogodila se pogreska pri dohvacanju korisnika id=" + $stateParams.id);
            });
        
    $scope.deleteUser = function(id) {

        var confirmation = confirm('Are you sure you want to delete this user?');
        if (confirmation) {

            $http({
                method: 'DELETE',
                url: backendBaseUrl + 'users/' + id})
                    .success(function(data, status, headers, config) {
                        alert("Korisnik je izbrisan");

                        for (var i = 0; i < $scope.users.length; i++) {
                            if ($scope.users[i].id === id) {
                                $scope.users.splice(i, 1);
                            }
                        }
                    })
                    .error(function(data, status, headers, config) {
                        alert("Dogodila se pogreska brisanju korisnika id=" + id);
                    })
                    ;
        }
    };



}]);

myapp.controller('membersEditController', ['$scope', '$http', '$stateParams', function($scope, $http, $stateParams) {

    //there is no perfect-scrollbar because controller is child of membersDetailsController

    $scope.editUser = function(id) {

        var putData = new Object();
        putData.name = $scope.user.name;
        putData.surname = $scope.user.surname;
        putData.gender = $scope.user.gender;
        putData.birth = $scope.user.birth;
        putData.email = $scope.user.email;
        putData.phone = $scope.user.phone;
        putData.city = $scope.user.city;
        putData.state = $scope.user.state;
	putData.base64image = $scope.uploadme;
        putData.profession = $scope.user.profession;
        putData.relationship = $scope.user.relationship;
        putData.weight = $scope.user.weight;
        putData.history = $scope.user.history;
        putData.remarks = $scope.user.remarks;
        putData.notes = $scope.user.notes;
        //putData.age = $scope.user.age;    //STARA VRIJEDNOST se učitava??
        
        //alert($scope.user.age);

        $http({method: 'PUT',
            url: backendBaseUrl + 'users/' + id,
            data: putData})
                .success(function(data, status, headers, config) {
                    $scope.user.image = data[0].image;
                    //putData.id = id; // surplus because push(data[0])
                    alert("Korisnik je izmjenjen"); 
                    
                    for (var i = 0; i < $scope.users.length; i++) {
                        if ($scope.users[i].id === id) {
                            $scope.users.splice(i, 1);
                            $scope.users.push(data[0]);
                        }
                    }                 

                })

                .error(function(data, status, headers, config) {
                    console.log("Niste unijeli sve podatke/greska pri dodavanju korisnika");
                });
    };

}]);
