var myapp = angular.module('trenkk', ['ui.bootstrap', 'ui.router', 'ngTable']);

myapp.run(function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;             // for ng-class={active: $state.includes()};
    $rootScope.$stateParams = $stateParams;
});

myapp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider
            //.when("/c?id", "/members/:id")
            .otherwise("/");
     
    $stateProvider
            .state('dashboard', {
                url: '/',
                templateUrl: 'templates/test.html'
            })
            
            .state('members', {
                url: '/members',
                templateUrl: 'templates/members.html',
                controller: 'membersController'
            })
            
            .state('members.new', {
                parent: "members",
                url:"/new",
                templateUrl: "templates/members.new.html",
                controller: 'membersNewController'
            })
            
            // POPRAVI GREŠKU OKO URL-A, UI-STATE=".DETAIL"
            .state('members.detail', {
                parent: "members",
                url: "/:id",
                templateUrl: "templates/members.detail.html",
                controller: 'membersDetailsController'
               /*controller: function($scope, $stateParams){
                    console.log($stateParams.id);
                }
               */
            })
            
            .state('members.detail.edit', {
                parent: 'members.detail',
                url:"/edit",
                views: {
                    'detail': {
                         templateUrl: "templates/members.detail.edit.html",
                         controller: 'membersEditController'
                    }
                }
    
            })
            
            //TABS TESTING      
            .state('members.detail.profile', {
                parent: 'members.detail',
                url: '/profile',
                views: {
                    'items': {
                        templateUrl: 'templates/tabs/tab1-profile.html',
                        controller: 'Profile'
                    }
                }
            })
            
            .state('members.detail.fees', {
                    parent: 'members.detail',
                    url: '/fees',
                    views: {
                      'items': {
                        templateUrl: 'templates/tabs/tab2-fees.html',
                        controller: 'Fees'
                        /*controller: function($scope, $stateParams) {
                            console.log($stateParams.id);
                        } 
                        */ 
                      }
                    }
            })
                
            .state('members.detail.notes', {
                    parent: 'members.detail',
                    url: '/notes',
                    views: {
                      'items': {
                        templateUrl: 'templates/tabs/tab3-notes.html',
                        controller: 'Notes'
                      }
                    }
            })
     ;
     
});

