var backendBaseUrl = "http://localhost/app/public_html/backend/rest.php/";

myapp.controller('Profile', function($scope) {
    $scope.content = "Sadrzaj1...";
});

myapp.controller('Fees', ['$scope', '$http', '$stateParams', '$modal',  function($scope, $http, $stateParams, $modal) {

        // GET ALL INVOICES //
        //console.log($stateParams.id);

        $http({
            method: 'GET',
            url: backendBaseUrl + 'users/' + $stateParams.id + '/subscriptions'})

                .success(function(data, status, headers, config) {
                    $scope.subscriptions = data;
                })
                .error(function(data, status, headers, config) {
                    alert("Dogodila se pogreska pri dohvacanju fakture s korisnickim id=" + $stateParams.id);
                });

        $scope.open = function() {
            var modalInstance = $modal.open({
                templateUrl: 'templates/tabs/modal.html',
                controller: 'ModalInstanceCtrl',
                resolve: {
                    test: function() {                  //resolving problem how to pass parameter to controller
                        //console.dir($scope.user); 
                        return $scope.user;
                    },
                    test2: function() {                  //resolving problem how to pass parameter to controller
                        //console.dir($scope.subscriptions); 
                        return $scope.subscriptions;

                    }
                }
            });

            //console.dir($scope.subscriptions);  //array
            //console.dir($scope.user);           //object

        };
    }]);

myapp.controller('Notes', function($scope) {
    $scope.content = "Sadrzaj3...";
});
