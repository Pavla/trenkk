//var backendBaseUrl = "http://localhost/app3-test/public_html/backend/rest.php/";

myapp.controller('ModalInstanceCtrl', ['$scope', '$http', '$modalInstance', '$stateParams', 'test', 'test2', function($scope, $http, $modalInstance, $stateParams, test, test2) {

        $scope.user = test;                 //resolving problem how to pass parameter from modal
        //console.dir($scope.user.name);
        $scope.id_users = $stateParams.id;      // for easy use
        
        $scope.subscriptions = test2;           // for updating/push table content      
        //console.dir($scope.subscriptions);

        $scope.input = {};   //little trick to bind a scope, single object (array with [] )
        
        //here we testing moment.js for parsing,validating and manipulating dates 
        $scope.todaysDate = moment().format('DD/MM/YYYY');   //moment.js   
        $scope.input.dateSubscription = $scope.todaysDate;   // trick to set up todays date

        $scope.typeNames = [
            {name: 'Regular', value: 'regular'}, 
            {name: 'Student', value: 'student'}, 
            {name: 'Free', value: 'free'}
        ];
        
        $scope.input.typeSubscription = $scope.typeNames[0].valueOf(); //trick to set up default selection value
        
        //if it's free, then it's paid
        $scope.selectedTypeName = $scope.typeNames[2].name;
  
        $scope.input.paidSubscription = 0;         // trick to set from undefined to 0
        $scope.end_date = null;
        
        $scope.ok = function() {
            //we set year by todays-year because of option of free payment 
            $scope.todaysYear = ($scope.input.dateSubscription).slice(-4);      //trick for using year value

            var postDataInv = new Object();         //create an object
            postDataInv.id_users = $scope.id_users;    
            postDataInv.year = $scope.todaysYear;   // if we needed

            postDataInv.start_date = $scope.input.dateSubscription;
           
                //calculate +30 days, and payment delay
                var paidDate = $scope.input.dateSubscription;
                var nextPaidDate = moment(moment(paidDate, 'DD/MM/YYYY').add('days', 30)).format('MM/DD/YYYY');   //nextPaidDate has full format date                 

                var today = moment(moment($scope.todaysDate, 'DD/MM/YYYY')).format('MM/DD/YYYY');  //to get full date again
                // we formatted days of today, and nextPaidDate to MM/DD/YYYY
                //cause of calculating date diff, which includes that format

                var dateB = moment(nextPaidDate);
                var dateC = moment(today);

            // (paidDate+30)post
            $scope.end_date = moment(nextPaidDate).format('DD/MM/YYYY');
            postDataInv.end_date = $scope.end_date;
            
            //calculation ((paidDate+30)- today's date)
            $scope.paymentdelay = dateB.diff(dateC, 'days');
            postDataInv.paymentdelay = $scope.paymentdelay;


            postDataInv.type = $scope.input.typeSubscription.name;
                     
            //if it free, then it's paid
            if( postDataInv.type === $scope.selectedTypeName){
                postDataInv.paid = '1';
            }
//            else
//            {   
//                // IF PAID IS 0, DATE WILL BE "-"
//                if($scope.input.paidSubscription == 0){
//                    postDataInv.paid = $scope.input.paidSubscription; //unpaid returns undentified
//                    postDataInv.date = "-";
//                }
//                else{
//                    postDataInv.paid = $scope.input.paidSubscription;
//                    postDataInv.start_date = $scope.input.dateSubscription;
//                    
//                    //calculate +30 days, and payment delay
//                    var paidDate = $scope.input.dateSubscription;
//                    var nextPaidDate = moment(moment(paidDate, 'DD/MM/YYYY').add('days', 30)).format('MM/DD/YYYY');   //nextPaidDate has full format date                 
//                    
//                    var today = moment(moment($scope.todaysDate, 'DD/MM/YYYY')).format('MM/DD/YYYY');  //to get full date again
//                    // we formatted days of today, and nextPaidDate to MM/DD/YYYY
//                    //cause of calculating date diff, which includes that format
//                    
//                    var dateB = moment(nextPaidDate);
//                    var dateC = moment(today); 
//                    
//                    // (paidDate+30)post
//                    $scope.end_date = moment(nextPaidDate).format('DD/MM/YYYY');
//                    postDataInv.end_date = $scope.end_date;
//               
//                    
//                    //calculation ((paidDate+30)- today's date)
//                    $scope.paymentdelay = dateB.diff(dateC, 'days'); 
//                    postDataInv.paymentdelay = $scope.paymentdelay;
//                }
//            }
            //console.log(postDataInv);

            $http({method: 'POST',
                url: backendBaseUrl + 'subscriptions',
                data: postDataInv})
                    .success(function(data, status, headers, config) {
                        alert("Dodana je nova faktura");
                        $scope.subscriptions.push(data[0]);
                    })
                    .error(function(data, status, headers, config) {
                        console.log("Niste unijeli sve podatke/greska pri dodavanju fakture");
                    });


            $modalInstance.close();
            //alert('ok');

        };

        $scope.cancel = function() {
            $modalInstance.dismiss('cancel');

        };



    }]);
