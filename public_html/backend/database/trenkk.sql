-- phpMyAdmin SQL Dump
-- version 4.1.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 18, 2014 at 09:24 PM
-- Server version: 5.5.35-0ubuntu0.12.04.2
-- PHP Version: 5.3.10-1ubuntu3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trenkk`
--

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(80) NOT NULL AUTO_INCREMENT,
  `id_users` int(11) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `type` enum('regular','student','free') DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `start_date` varchar(100) DEFAULT NULL,
  `end_date` varchar(100) DEFAULT NULL,
  `paymentdelay` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `subscription_user` (`id_users`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin2 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `name` varchar(100) CHARACTER SET latin2 NOT NULL,
  `surname` varchar(100) CHARACTER SET latin2 NOT NULL,
  `gender` enum('male','female') CHARACTER SET latin2 NOT NULL,
  `birth` varchar(100) CHARACTER SET latin2 NOT NULL,
  `email` varchar(100) CHARACTER SET latin2 NOT NULL,
  `phone` varchar(100) CHARACTER SET latin2 NOT NULL,
  `city` varchar(100) CHARACTER SET latin2 NOT NULL,
  `state` varchar(100) CHARACTER SET latin2 NOT NULL,
  `image` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `profession` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `relationship` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `weight` varchar(100) CHARACTER SET latin2 DEFAULT NULL,
  `history` varchar(500) CHARACTER SET latin2 DEFAULT NULL,
  `remarks` varchar(500) CHARACTER SET latin2 DEFAULT NULL,
  `notes` varchar(500) CHARACTER SET latin2 DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=672 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `subscription_user` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
