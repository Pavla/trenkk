<?php

error_reporting(E_ALL);

// load required files
require ('./Slim/Slim.php');
require ('./RedBean/rb.phar');

// register Slim auto-loader
\Slim\Slim::registerAutoloader();

// set up database connection
R::setup('mysql:host=localhost; dbname=trenkk', 'root', 'seget');
R::freeze(true);

// initialize app
$app = new \Slim\Slim();

class ResourceNotFoundException extends Exception {
    
}

//handle put requests
$app->put('/users/:id', function ($id) use ($app) {
    try {
        // get and decode JSON request body
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);

        // query database for single member
        $user = R::findOne('users', 'id=?', array($id));

        // store modified members
        // return JSON-encoded response body
        
        if ($user) {
            // edit avatar photo
            if(array_key_exists ( "base64image" , $input )){
                    $imagebase = './testimages/';
                    if ($user->image != 'default_avatar.jpg') {
                        unlink($imagebase . $user->image);
                    }
                    $image = uniqid() . '.jpeg';
                    $img = $input->base64image;
                    $img = str_replace('data:image/jpeg;base64,', '', $img);
                    $img = str_replace(' ', '+', $img);
                    $data = base64_decode($img);
                    $file = './testimages/' . $image;
                    $success = file_put_contents($file, $data);
                    $user->image = $image;
            }
            // member info
            $user->name = (string) $input->name;
            $user->surname = (string) $input->surname;
            $user->gender = (string) $input->gender;
            $user->birth = (string) $input->birth;
            $user->email = (string) $input->email;
            $user->phone = (string) $input->phone;
            $user->city = (string) $input->city;
            $user->state = (string) $input->state;
            $user->profession = (string) $input->profession;
            $user->relationship = (string) $input->relationship;
            $user->weight = (string) $input->weight;
            $user->history = (string) $input->history;
            $user->remarks = (string) $input->remarks;
            $user->notes = (string) $input->notes;
            R::store($user);
            
            $app->response()->header('Content-Type', 'application/json');
            echo json_encode(R::exportAll($user));
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});

//handle delete requests
$app->delete('/users/:id', function ($id) use ($app) {
    try {
        // query database for article
        $request = $app->request();
        $user = R::findOne('users', 'id=?', array($id));

        // delete avatar and member
        if ($user) {
            $imagebase = './testimages/';
	    if($user->image != 'default_avatar.jpg')
	    	unlink($imagebase . $user->image);
            
            R::trash($user);
            $app->response()->status(204);
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});



//handle post requests
$app->post('/users', function () use ($app) {
    try {
        // get and decode JSON request body
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);
        
        // uploading avatar
	$image = uniqid() . '.jpeg';

	if(! array_key_exists ( "base64image" , $input )){
		$image = 'default_avatar.jpg';
	}else{
		$img = $input->base64image;		
		$img = str_replace('data:image/jpeg;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file = './testimages/'. $image;
		$success = file_put_contents($file, $data);
	}
	
        // store member record
        $user = R::dispense('users');
        $user->name = (string) $input->name;
        $user->surname = (string) $input->surname;
        $user->gender = (string) $input->gender;
        $user->birth = (string) $input->birth;
        $user->email = (string) $input->email;
        $user->phone = (string) $input->phone;
        $user->city = (string) $input->city;
        $user->state = (string) $input->state;
	$user->image = $image;
        
        if(array_key_exists("profession", $input)){
            $user->profession = (string) $input->profession;
        }
        else {
            $user->profession = NULL;   //in sql this field allows null value
        }
        
        if(array_key_exists("relationship", $input)){
            $user->relationship = (string) $input->relationship;
        }
        else {
            $user->relationship = NULL; //in sql this field allows null value
        }
        
        if(array_key_exists("weight", $input)){
           $user->weight = (string) $input->weight;
        }
        else {
            $user->weight = NULL; //in sql this field allows null value
        }
        
        if(array_key_exists("history", $input)){
            $user->history = (string) $input->history;
        }
        else {
            $user->history = NULL; //in sql this field allows null value
        }
        
        if(array_key_exists("remarks", $input)){
            $user->remarks = (string) $input->remarks;
        }
        else {
            $user->remarks = NULL; //in sql this field allows null value
        }
        
        if(array_key_exists("notes", $input)){
             $user->notes = (string) $input->notes;
        }
        else {
            $user->notes = NULL; //in sql this field allows null value
        }

        $id = R::store($user);

        // return JSON-encoded response body
        $app->response()->header('Content-Type', 'application/json');

	$user = R::findOne('users', 'id=?', array($id));
        echo json_encode(R::exportAll($user));
        //echo json_encode($user);
        
        //echo json_encode(R::exportAll($user));
        //this send users id when new user is added
        //echo json_encode($id);
    } 
    catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});

// handle GET requests for /users/:id
$app->get('/users/:id', function ($id) use ($app) {
    try {
        // query database for single article
        $user = R::findOne('users', 'id=?', array($id));

        // calculate age by user->birth
        $birthday = new DateTime();
        $birthday->setTimestamp(strtotime(str_replace('/', '-', $user->birth)));
        $now = new DateTime("now");
        $age = $birthday->diff($now);
        $user["age"] = $age->y;
        
        //var_dump($user);
        //die();
        
        
        if ($user) {
            // if found, return JSON response
            $app->response()->header('Content-Type', 'application/json');
            echo json_encode(R::exportAll($user));
        } else {
            // else throw exception
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        // return 404 server error
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});


// handle GET requests for /users
$app->get('/users', function () use ($app) {
    // query database for all articles
    $users = R::find('users');

    // send response header for JSON content type
    $app->response()->header('Content-Type', 'application/json');

    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($users));
});


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

// handle GET requests for /subscriptions
$app->get('/users/:id/subscriptions', function ($id) use ($app) {
    
    // query database for all articles
    //$users2 = R::getAll('select * from aaaddd');     
    
    // return JSON-encoded response body with query results
    //echo json_encode($users2);

        // query database for all articles
    $subscriptions = R::find('subscription','id_users=?', array($id));

    // send response header for JSON content type
    $app->response()->header('Content-Type', 'application/json');

    // return JSON-encoded response body with query results
    echo json_encode(R::exportAll($subscriptions));
    
});


// handle GET requests for /subscriptions/:id
$app->get('/subscriptions/:id', function ($id) use ($app) {
    try {
        // query database for single article
        $subscription = R::findOne('subscription', 'id=?', array($id));

        if ($subscription) {
            // if found, return JSON response
            $app->response()->header('Content-Type', 'application/json');
            echo json_encode(R::exportAll($subscription));
        } else {
            // else throw exception
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        // return 404 server error
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});

//handle delete requests
$app->delete('/subscriptions/:id', function ($id) use ($app) {
    try {
        // query database for article
        $request = $app->request();
        $subscription = R::findOne('subscription', 'id=?', array($id));

        // delete avatar and member
        if ($subscription) {
            R::trash($subscription);
            $app->response()->status(204);
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});

//handle put requests
$app->put('/subscriptions/:id', function ($id) use ($app) {
    try {
        // get and decode JSON request body
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);

        // query database for single member
        $subscription = R::findOne('subscription', 'id=?', array($id));

        // store modified members
        // return JSON-encoded response body
        
        if ($subscription) {
            
            // member info
            $subscription->id_users = (string) $input->id_users;
            $subscription->year = (string) $input->year;
            $subscription->type = (string) $input->type;
            $subscription->paid = (string) $input->paid;
            $subscription->start_date = (string) $input->start_date;
            $subscription->end_date = (string) $input->end_date;
            $subscription->paymentdelay = (string) $input->paymentdelay;
            
            R::store($subscription);
            
            $app->response()->header('Content-Type', 'application/json');
            echo json_encode(R::exportAll($subscription));
            
        } else {
            throw new ResourceNotFoundException();
        }
    } catch (ResourceNotFoundException $e) {
        $app->response()->status(404);
    } catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});

$app->post('/subscriptions', function () use ($app) {
    try {
        // get and decode JSON request body
        $request = $app->request();
        $body = $request->getBody();
        $input = json_decode($body);
        
        // store member record
        $subscription = R::dispense('subscription');
        
        if(array_key_exists("id_users", $input)){
           $subscription->id_users = (string) $input->id_users;
        }
        else {
            $subscription->id_users = "-"; //in sql this field allows null value
        }
        
        if(array_key_exists("year", $input)){
           $subscription->year = (string) $input->year;
        }
        else {
            $subscription->year = "-"; //in sql this field allows null value
        }
        
        if(array_key_exists("type", $input)){
           $subscription->type = (string) $input->type;
        }
        else {
            $subscription->type = "-"; //in sql this field allows null value
        }
        
        if(array_key_exists("paid", $input)){
           $subscription->paid = (string) $input->paid;
        }
        else {
            $subscription->paid = "-"; //in sql this field allows null value
        }
        
        if(array_key_exists("start_date", $input)){
           $subscription->start_date = (string) $input->start_date;
        }
        else {
            $subscription->start_date = "-"; //in sql this field allows null value
        }
        
         if(array_key_exists("end_date", $input)){
           $subscription->end_date = (string) $input->end_date;
        }
        else {
            $subscription->end_date = "-"; //in sql this field allows null value
        }
        
        if(array_key_exists("paymentdelay", $input)){
           $subscription->paymentdelay = (string) $input->paymentdelay;
        }
        else {
            $subscription->paymentdelay = "-"; //in sql this field allows null value
        }

        $id = R::store($subscription);

        // return JSON-encoded response body
        $app->response()->header('Content-Type', 'application/json');

	$subscription = R::findOne('subscription', 'id=?', array($id));
        echo json_encode(R::exportAll($subscription));
        //echo json_encode($user);
        
        //echo json_encode(R::exportAll($user));
        //this send users id when new user is added
        //echo json_encode($id);
    } 
    catch (Exception $e) {
        $app->response()->status(400);
        $app->response()->header('X-Status-Reason', $e->getMessage());
    }
});



// run

$app->run();
?>
